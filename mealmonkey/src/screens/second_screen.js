import React from 'react';
import { ImageBackground, Image, StyleSheet, Dimensions, View, Text } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';



const Deviceheight = Dimensions.get("window").height;
const Devicewidth = Dimensions.get("window").width;

const second_screen = ({navigation}) => {

  return (


    <View style={styles.container}>

      <View style={styles.top}>
        <ImageBackground style={{ width: '100%', height: '117%' }}
          source={require('../assets/top.png')}
        />
      </View>
      <View style={styles.logo}>
        <Image style={{ width: '90%', height: '80%' }}
          source={require('../assets/Logo.png')}
        />
      </View>
      <View style={{ alignItems: "center", marginTop: 10 }}>
        <Text >Discover the best foods from over1,000</Text>
        <Text>restruants and fast delivery to your doorsteps</Text>
      </View>
        <View style={{marginTop:30}}>
      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('login_screen')}>
        <Text style={{ color: "#fff" }}>Login</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('signup_screen')}>
      
        <Text style={{ color: "#fff" }}
        >Create an account</Text>
      </TouchableOpacity>
      </View>
    </View>




  );
}

export default second_screen;
const styles = StyleSheet.create({
  container: {
    // backgroundColor: "yellow",
    height: Deviceheight, width: Devicewidth,
    // alignItems:"flex-start"
  },
  top: {
    height: Deviceheight / 2.2,
    width: Devicewidth, paddingBottom: 2
    // backgroundColor:"red"
  },
  logo: {
    height: Deviceheight / 4,
    width: Devicewidth / 2,
    borderRadius: 30,
    // backgroundColor: "grey",
    justifyContent: "center",
    marginLeft: 100,
    alignItems: "center",
    // marginBottom: 40
  },
  button: {
    height: Deviceheight / 15,
    width: Devicewidth / 1.4,
    backgroundColor: "orange",
    borderRadius: 40,
    alignSelf: "center",
    marginTop:10,
    alignItems: "center",
    justifyContent: "center"


  }

})