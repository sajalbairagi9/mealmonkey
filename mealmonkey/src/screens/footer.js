import React, { useState } from 'react';
import { Text, TouchableOpacity, StyleSheet, Dimensions, View } from 'react-native';

import MenuIcon from 'react-native-vector-icons/Entypo';
import OfferIcon from 'react-native-vector-icons/FontAwesome';
import HomeIcon from 'react-native-vector-icons/Entypo';
import ProfileIcon from 'react-native-vector-icons/FontAwesome';
import MoreIcon from 'react-native-vector-icons/MaterialIcons';

const Deviceheight = Dimensions.get("window").height;
const Devicewidth = Dimensions.get("window").width;

const footer = () => {
    const [Menu, setMenu] = useState(false);
    const [Offer, setOffer] = useState(false);
    const [Home, setHome] = useState(false);
    const [user, setUser] = useState(false);
    const [More, setMore] = useState(false);

    const handleMenupress = () => {
        setMenu(true)
        setOffer(false)
        setHome(false)
        setUser(false)
        setMore(false)
    }
    const handlebagpress = () => {
        setMenu(false)
        setOffer(true)
        setHome(false)
        setUser(false)
        setMore(false)
    }
    const handleHomepress = () => {
        setMenu(false)
        setOffer(false)
        setHome(true)
        setUser(false)
        setMore(false)
    }
    const handleUserpress = () => {
        setMenu(false)
        setOffer(false)
        setHome(false)
        setUser(true)
        setMore(false)
    }
    const handleMenuopenpress = () => {
        setMenu(false)
        setOffer(false)
        setHome(false)
        setUser(false)
        setMore(true)
    }

    return (
        <View style={{ flexDirection: "row", alignItems: "center", width: Devicewidth, justifyContent: "space-around", paddingHorizontal: 10, alignSelf: "center" }}>

            {Menu == true && Offer == false && Home == false && user == false && More == false ?
                <View style={styles.aftertouchview} >
                    <MenuIcon name="menu" size={30} color="#FFA500" />
                    <Text style={styles.afterText}>Menu</Text>
                </View>
                :
                <TouchableOpacity style={styles.beforetouchview} onPress={() => handleMenupress()}>
                    <MenuIcon name="menu" size={30} color="#949494" />
                    <Text style={styles.beforeText}>Menu</Text>
                </TouchableOpacity>
            }

            {Menu == false && Offer == true && Home == false && user == false && More == false ?
                <View style={styles.aftertouchview} >
                   <OfferIcon name="shopping-bag" size={25} color="#FFA500" />
                    <Text style={styles.afterText}>Offer</Text>
                </View>
                :
                <TouchableOpacity style={styles.beforetouchview} onPress={() => handlebagpress()}>
                    <OfferIcon name="shopping-bag" size={25} color="#949494" />
                    <Text style={styles.beforeText}>Offer</Text>
                </TouchableOpacity>
            }

            {Menu == false && Offer == false && Home == true && user == false && More == false ?
                <View style={styles.aftertouchview} >
                   <HomeIcon name="home" size={30} color="#FFA500" />
                    <Text style={styles.afterText}>Home</Text>
                </View>
                :
                <TouchableOpacity style={styles.beforetouchview} onPress={() => handleHomepress()}>
                    <HomeIcon name="home" size={30} color="#949494" />
                    <Text style={styles.beforeText}>Home</Text>
                </TouchableOpacity>
            }

            {Menu == false && Offer == false && Home == false && user == true && More == false ?
                <View style={styles.aftertouchview} >
                    <ProfileIcon name="user" size={30} color="#FFA500" />
                    <Text style={styles.afterText}>Profile</Text>
                </View>
                :
                <TouchableOpacity style={styles.beforetouchview} onPress={() => handleUserpress()}>
                    <ProfileIcon name="user" size={30} color="#949494" />
                    <Text style={styles.beforeText}>Profile</Text>
                </TouchableOpacity>
            }

            {Menu == false && Offer == false && Home == false && user == false && More == true ?
                <View style={styles.aftertouchview} >
                     <MoreIcon name="menu-open" size={30} color="#FFA500" />
                    <Text style={styles.afterText}>More</Text>
                </View>
                :
                <TouchableOpacity style={styles.beforetouchview} onPress={() => handleMenuopenpress()}>
                    <MoreIcon name="menu-open" size={30} color="#949494" />
                    <Text style={styles.beforeText}>More</Text>
                </TouchableOpacity>
            }



        </View>
    );
}

export default footer
const styles = StyleSheet.create({
    beforetouchview: {
        height: Deviceheight / 15,
        width: Devicewidth / 8,
        //    backgroundColor: "yellow",
        alignItems: "center",
        justifyContent:'space-around'
    },
    aftertouchview: {
        height: Deviceheight / 20,
        width: Devicewidth / 8,
        //    backgroundColor: "yellow",
        alignItems: "center"
    },
    beforeText:{
        fontSize:12,
        color:"#949494",
        textAlign:"center"
    },
    afterText:{
        fontSize:12,
        color:"#FFA500",
        textAlign:"center"
    }
})
