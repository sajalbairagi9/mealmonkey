import { NavigationContainer } from '@react-navigation/native';
import React, {useState} from 'react';

// import all the components we are going to use
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  Image,
} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';

const App = ({navigation}) => {
  const [showRealApp, setShowRealApp] = useState(false);

  const onDone = () => {
    navigation.push("auth_navigator")
    
  };
  // const onSkip = () => {
  //   setShowRealApp(true);
  // };

  const RenderItem = ({item}) => {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: item.backgroundColor,
          alignItems: 'center',
          paddingBottom: 100,
        }}>
        
        <Image
          style={styles.introImageStyle}
          source={item.image} />

          <Text style={styles.introTitleStyle}>
          {item.title}
        </Text>

        <Text style={styles.introTextStyle}>
          {item.text}
        </Text>

      </View>
    );
  };

  return (
    <>
      {showRealApp ? (
        <SafeAreaView style={styles.container}>
          <View style={styles.container}></View>
        </SafeAreaView>
      ) : (
        <AppIntroSlider
          data={slides}
          renderItem={RenderItem}
          onDone={onDone}
          showSkipButton={false}
          bottomButton
          doneLabel={"Next"}
          activeDotStyle={{ backgroundColor: "orange" }}
        />
      )}
    </>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
  },
  titleStyle: {
    textAlign: 'center',
    fontSize: 10,
    fontWeight: 'bold',
  },
  paragraphStyle: {
    // padding: 20,
    textAlign: 'center',
    fontSize: 10,
  },
  introImageStyle: {
   marginTop:70
  },
  introTextStyle: {
    color: '#000',
    // paddingVertical: 30,
    alignSelf: "center", textAlign: "center", fontSize: 15, marginTop:15
  },
  introTitleStyle: {
    fontSize: 18,
    color: '#000',
    textAlign: 'center',
    marginTop:20
    
  },
 
});

const slides = [
  {
    key: 's1',
    image: require('../assets/first.png'),
    text:  '   Discover the best foods over1000  \nresturants and fast deleveryto your doorsteps',
    title: 'Find Food you Love',
    backgroundColor: '#fff',
  },
  {
    key: 's2',
    title: 'Fast Delevery',
    text: '   Fast food deleveryto your home,office\nwhereever you are',
    image: require('../assets/second.png'),
    backgroundColor: '#fff',
  },
  {
    key: 's3',
    title: 'Live Tracking',
    text:  '  real time tracking of your foodon the app\nonce you placedthe order',
    image: require('../assets/third.png'),
    backgroundColor: '#fff',
  },
  
  
  
];