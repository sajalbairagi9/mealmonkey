import React, { useEffect } from 'react';
import { Image, ImageBackground, Dimensions, View } from 'react-native';




const Deviceheight = Dimensions.get("window").height;
const Devicewidth = Dimensions.get("window").width;


const splash_screen = ({navigation}) => {
 

  useEffect(() => {
    setTimeout(()=>{
      navigation.navigate("first_screen")
     },2000)
}, [])

  return (
    <View style={{ height: Deviceheight, width: Devicewidth }}>
      <ImageBackground
        source={require('../assets/Back.png')}
        resizeMode="cover">

        <View style={{ height: Deviceheight, width: Devicewidth, justifyContent: "center" }}>

          <View style={{ height: Deviceheight / 3, width: Devicewidth / 1.3, alignSelf: "center" }}>
            <Image style={{ alignSelf: "center" }}
              source={require('../assets/Logo.png')}
            />
          </View>

        </View>

      </ImageBackground>
    </View>

  )
}

export default splash_screen;