import React, { useState } from 'react';
import { View, SafeAreaView, Text, Dimensions, StyleSheet, TextInput } from "react-native";
import { TouchableOpacity } from 'react-native-gesture-handler';
import { postApiCall } from "../screens/api";
const Deviceheight = Dimensions.get("window").height;
const Devicewidth = Dimensions.get("window").width;

const login_screen = ({ navigation }) => {
  const [text, setText] = useState('');
  const [password, setPassword] = useState('');

  const handlesubmit = async () => {
    // const object = {
    //   email: text,
    //   password: password
    // }

    // console.log('my login object', object)
    // await postApiCall(`api/auth/login`, object)
    //         .then(response => {
    //             console.log('my login res',response)
    //         })
    //         .catch(error => {
    //             console.error(error)
    //         })
    navigation.push("home_navigator")  
  };

  return (
    <SafeAreaView>
      <View>
        <View style={{ height: Deviceheight / 8, width: Devicewidth / 2, marginTop: 70, alignSelf: "center", alignItems: "center" }}>
          <Text style={{ alignItems: "center", fontSize: 30 }}>Login</Text>
          <Text style={{ alignItems: "center", fontSize: 16, marginTop: 10 }}>Add your Details to login</Text>
        </View>

        {/* text input */}
        <View style={{ height: Deviceheight / 2.5, width: Devicewidth / 1.2, marginTop: 70, alignSelf: "center", alignItems: "center", justifyContent: "space-evenly" }}>
          <TextInput
            style={styles.name}
            placeholder={" Your Email"}
            placeholderTextColor={"#9CA3AF"}
            onChangeText={val => setText(val)}

            value={text}
          />

          <TextInput
            style={styles.name}
            placeholder={" Password"}

            placeholderTextColor={"#9CA3AF"}
            onChangeText={val => setPassword(val)}
            Value={password}
          />
          <TouchableOpacity style={styles.button} onPress={() => handlesubmit()} >
            <Text style={{ color: "#fff" }}>Login</Text>

          </TouchableOpacity>

        </View>
        <View style={{ height: Deviceheight / 19, width: Devicewidth / 2, alignSelf: "center", alignItems: "center", }}>
          <TouchableOpacity>
            <Text>Forgot your Password?</Text>
          </TouchableOpacity>
        </View>
        <View style={{ height: Deviceheight / 19, width: Devicewidth / 1.8, alignSelf: "center", alignItems: "center", flexDirection: "row" }}>
          <Text> Dont have any account?</Text>
          <TouchableOpacity>
            <Text style={{ fontWeight: "bold", color: "orange" }} onPress={() => navigation.navigate('signup_screen')}>SignUp</Text>
          </TouchableOpacity>
        </View>


      </View>
    </SafeAreaView>
  );
}

export default login_screen;
const styles = StyleSheet.create({
  name: {
    height: Deviceheight / 15,
    width: Devicewidth / 1.2,
    alignItems: "center",
    color: "#000",
    borderColor: "#000",
    borderWidth: 0.1,
    borderRadius: 30,
    backgroundColor: "#D4D2D2"
  },
  button: {
    height: Deviceheight / 15,
    width: Devicewidth / 1.2,
    backgroundColor: "orange",
    borderRadius: 40,
    alignSelf: "center",
    marginTop: 10,
    alignItems: "center",
    justifyContent: "center"
  }
})