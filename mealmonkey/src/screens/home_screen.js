import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, Dimensions, View, Text, Image, ImageBackground, } from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import Footer from '../screens/footer';

const Deviceheight = Dimensions.get("window").height;
const Devicewidth = Dimensions.get("window").width;

const home_screen = () => {
  const [text, setText] = useState('');
  return (
    <SafeAreaView>
      <View style={{backgroundColor:"#fff"}}>
        {/* header */}
        <View style={styles.header}>
          <Text style={styles.headertext}>Menu</Text>
          <Image source={require('../assets/cart.png')}></Image>

        </View>

        {/* search food section */}
        <View style={styles.searchfood}>
          <View style={{flexDirection:"row",backgroundColor:"#D4D2D2",height: Deviceheight / 14,
    width: Devicewidth / 1.2,borderRadius:30,alignSelf:"center",justifyContent:"space-between",paddingHorizontal:5}}>
          <View style={styles.searchicon}>
                <Image style={styles.icon}
                  source={require('../assets/search.png')}
                />
              </View>
          <TextInput
           style={styles.searchfoodtextinput}
          placeholder={" Search food"}
            placeholderTextColor={"#9CA3AF"}
            // onChangeText={text => setText(text)}
            // value={text}
          />
          </View>
          

        </View>

        {/* food section */}

        <View style={styles.showfood}>
          <ImageBackground style={{ width: '50%', height: '100%', }}
            source={require('../assets/slide.png')}>
            <View style={styles.perfood}>
              <View style={styles.foodimage}>
                <Image style={styles.imagefood}
                  source={require('../assets/Food.png')}></Image>
              </View>
              <View style={styles.foodname}>
                <Text style={styles.foodtext}>Food</Text>
                <Text style={styles.fooditemtext}>120 Items</Text>
              </View>
              <View style={styles.iconview}>
                <Image style={styles.icon}
                  source={require('../assets/arrow.png')}
                />
              </View>

            </View>


            <View style={styles.perfood}>
              <View style={styles.foodimage}>
                <Image style={styles.imagefood}
                  source={require('../assets/Beverage.png')}></Image>
              </View>
              <View style={styles.foodname}>
                <Text style={styles.foodtext}>Beverage</Text>
                <Text style={styles.fooditemtext}>220 Items</Text>
              </View>
              <View style={styles.iconview}>
                <Image style={styles.icon}
                  source={require('../assets/arrow.png')}
                />
              </View>
            </View>

            <View style={styles.perfood}>
              <View style={styles.foodimage}>
                <Image style={styles.imagefood}
                  source={require('../assets/Desserts.png')}></Image>
              </View>
              <View style={styles.foodname}>
                <Text style={styles.foodtext}>Desserts</Text>
                <Text style={styles.fooditemtext}>120 Items</Text>
              </View>
              <View style={styles.iconview}>
                <Image style={styles.icon}
                  source={require('../assets/arrow.png')}
                />
              </View>
            </View>

            <View style={styles.perfood}>
              <View style={styles.foodimage}>
                <Image style={styles.imagefood}
                  source={require('../assets/Promotions.png')}></Image>
              </View>
              <View style={styles.foodname}>
                <Text style={styles.foodtext}>Promotions</Text>
                <Text style={styles.fooditemtext}>220 Items</Text>
              </View>
              <View style={styles.iconview}>
                <Image style={styles.icon}
                  source={require('../assets/arrow.png')}
                />
              </View>
            </View>

          </ImageBackground>


        </View>

        {/* footer */}
        <View style={styles.footer}>
           <Footer/>
        </View>

      </View>
    </SafeAreaView>


  );
}

export default home_screen;
const styles = StyleSheet.create({

  header: {
    height: Deviceheight / 15,
    width: Devicewidth,
    // backgroundColor: "yellow",
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"space-between",
    paddingHorizontal:13
  },
  searchfood: {
    height: Deviceheight / 13,
    width: Devicewidth,
    // backgroundColor: "red",
    marginTop: 10
  },
  showfood: {
    height: Deviceheight / 1.5,
    width: Devicewidth,
    // backgroundColor:"grey",
    marginTop: 20,
    marginBottom:20,
    justifyContent:"space-evenly"
  },
  footer: {
    height: Deviceheight / 8,
    width: Devicewidth,
    // backgroundColor: "red",
    alignItems:"center",
    justifyContent:"center"
  },
  perfood: {
    height: Deviceheight / 8,
    width: Devicewidth / 1.3,
    backgroundColor: "#FFF",
    elevation: 10,
    marginTop: 20,
    marginLeft: 50,
    borderTopLeftRadius: 50,
    borderBottomLeftRadius: 50,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
  },
  foodimage: {
    height: Deviceheight / 8,
    width: Devicewidth / 4,
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    left: -40
  },
  imagefood: {
    width: '100%',
    height: '100%',
    resizeMode: "contain"
  },

  foodname: {
    flexDirection: "column",
    alignSelf: "center",
    // backgroundColor:"grey",
    marginRight: 10,
    width:Devicewidth/2.5,
  },
  foodtext: {
    fontWeight: "bold",
    fontSize: 20,
    textAlign: "left"
  },
  fooditemtext: {
    fontWeight: "100",
    fontSize: 10,
  },
  iconview: {
    width: Devicewidth / 12,
    height: Deviceheight / 22,
    borderRadius: 360,
    backgroundColor: "#fff",
    elevation: 5,
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    right: -15
  },
  icon: {
    width: '50%',
    height: '50%',
    resizeMode: "contain"
  },
  headertext:{
    textAlign:"center",
    fontWeight:"bold",
    fontSize:24
  },
  searchfoodtextinput:{
    height: Deviceheight / 14,
    width: Devicewidth / 1.45,
    alignItems: "center",
    color: "#000",
    borderRadius: 30,
    backgroundColor:"#D4D2D2",
    alignSelf:"center"
  },
  searchicon:{
    width: Devicewidth / 14,
    height: Deviceheight / 26,
    borderRadius: 360,
    // backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    alignSelf:"center"
    
  }



})