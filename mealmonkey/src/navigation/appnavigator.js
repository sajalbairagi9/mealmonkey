import React,{Component} from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import splash_navigator from "../navigation/splash_navigator";
import auth_navigator from "../navigation/auth_navigator";
import home_navigator from "../navigation/home_navigator";




const Stack = createStackNavigator();
export default class AppNavigator extends Component {
    render() {
        return (
            <Stack.Navigator screenOptions={{
                headerShown: false
              }}>
                  
                      <Stack.Screen name="splashNavigator" component={splash_navigator} initialRouteName="splashNavigator"/>
                      
                       <Stack.Screen name="auth_navigator" component={auth_navigator}/>
                      <Stack.Screen name="home_navigator" component={home_navigator}/>
              </Stack.Navigator>
        )
    }
}
