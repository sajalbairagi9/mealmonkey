import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import second_screen from '../screens/second_screen';
import login_screen from "../screens/login_screen"
import signup_screen from '../screens/signup_screen';





const Stack = createStackNavigator();
export default class auth_navigator extends Component {
    render() {
        return (
            <Stack.Navigator screenOptions={{
                headerShown: false
            }}>
                {
                    <>
                    <Stack.Screen name="second_screen" component={second_screen} initialRouteName="second_screen" />
                        <Stack.Screen name="signup_screen" component={signup_screen}/>
            

                        <Stack.Screen name="login_screen" component={login_screen} />


                    </>
                }
            </Stack.Navigator>
        )
    }
}
