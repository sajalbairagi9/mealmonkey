import React,{Component} from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import home_screen from "../screens/home_screen"






const Stack = createStackNavigator();
export default class home_navigator extends Component {
    render() {
        return (
            <Stack.Navigator screenOptions={{
                headerShown: false
              }}>
                  {
                      <>
                      <Stack.Screen name="home_screen" component={home_screen} initialRouteName="home_screen" />
                      </>
                  }
              </Stack.Navigator>
        )
    }
}
